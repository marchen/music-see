(function(){
		var canvas = document.querySelector('#canvasCircle');
		var stop2 = document.querySelector('#stop2');
		var windowW = document.documentElement.clientWidth;
		var windowH = document.documentElement.clientHeight;
		var aaa = document.querySelector('.aaaa');
		var array = new Uint8Array(analyser.frequencyBinCount);
		var stage = new createjs.Stage(canvas);
		var maxRadius = 600;
		var animateFlag = true;
		var radius = maxRadius;
		var shape = new createjs.Shape();
		var temp = 0;
		var color = radiusColor();
		stage.addChild(shape);
		function run(){
			if(animateFlag){
				requestAnimationFrame(run)
			}
			analyser.getByteFrequencyData(array)
			shape.graphics.clear();
			shape.graphics.setStrokeStyle(5,"round");
			for(var i = 128; i > 1; i--) {
				temp = (array[i]/128) == 0 ? 1 : (array[i]/128);
				shape.graphics.beginLinearGradientStroke(["#f00","#2FF"], [0, 1], 0, 80, 1200, 820).arc(600, 600, radius, 0, Math.PI * temp, false);
				radius = radius - 5;
			}
			radius = maxRadius;
			stage.update();
		}
		requestAnimationFrame(run);
		stop2.onclick = function(){
			if(animateFlag){
				animateFlag = false;
				this.innerHTML = '开始动画';
			}else{
				animateFlag = true;
				run();
				this.innerHTML = '停止动画';
			}
		}
		function radiusColor(){
			var r = Math.floor(Math.random()*256);
			var g = Math.floor(Math.random()*256);
			var b = Math.floor(Math.random()*256);
			return 'rgb(' + r + ',' + g + ',' + b + ')';
		}
		function radiusRadius(){
			return (Math.random()*2);
		}
	})();