var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');
/* GET home page. */
router.get('/', function(req, res, next) {
  fs.readdir(path.join(__dirname, '../public/music'),function(err, files){
  	if(err) {
  		console.log(err);
  	}else {
  		res.render('index', { "title": 'music-see', "lists": files});
  	}
  });
});

module.exports = router;
